//
//  InterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    override func awake(withContext context: Any?) {
    }
    
    func resetColor() {
        MyColorModel.hue = 1.0
        MyColorModel.saturation = 1.0
        MyColorModel.brightness = 1.0
    }
    
    override func willActivate() {
        resetColor()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }

}
