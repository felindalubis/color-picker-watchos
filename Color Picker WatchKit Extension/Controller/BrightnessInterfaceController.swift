//
//  BrightnessInterfaceController.swift
//  NanoChallenge WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit
import Foundation
import CoreData

class BrightnessInterfaceController: WKInterfaceController {
    @IBOutlet weak var group: WKInterfaceGroup!
    var dataManager = DataManager()
    var crownAccumulator = 0.0
    var brightVal: CGFloat = 1.0 {
        didSet {
            if brightVal < 0.0 || brightVal > 1.0 {
                brightVal = oldValue
            }
            group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: brightVal, alpha: 1))
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
    }
    
    override func willActivate() {
        super.willActivate()
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
        crownSequencer.focus()
    }
    
    override func didDeactivate() {
    }
    
    override func didAppear() {
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
    }
    
    override func willDisappear() {
        MyColorModel.brightness = brightVal
        crownSequencer.resignFocus()
    }
}

extension BrightnessInterfaceController: WKCrownDelegate {
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1 {
            brightVal += 0.05
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            brightVal -= 0.05
            crownAccumulator = 0.0
        }
    }
    
}

