//
//  ColorStarterInterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 25/08/21.
//

import WatchKit
import Foundation


class ColorStarterInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func didAppear() {
        MyColorModel.saturation = 1
        MyColorModel.brightness = 1
    }

    @IBAction func redButtonTapped() {
        MyColorModel.hue = 1
        toSaturationVC()
    }
    
    @IBAction func orangeButtonTapped() {
        MyColorModel.hue = 0.089
        toSaturationVC()
    }
    
    @IBAction func yellowButtonTapped() {
        MyColorModel.hue = 0.1639
        toSaturationVC()
    }
    
    @IBAction func purpleButtonTapped() {
        MyColorModel.hue = 0.7444
        toSaturationVC()
    }
    
    @IBAction func blueButtonTapped() {
        MyColorModel.hue = 0.6444
        toSaturationVC()
    }
    
    @IBAction func greenButtonTapped() {
        MyColorModel.hue = 0.333
        toSaturationVC()
    }
    
    func toSaturationVC() {
        self.pushController(withName: "saturation", context: self)
    }
}
