//
//  SaveColorInterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 24/08/21.
//

import WatchKit
import Foundation


class SaveColorInterfaceController: WKInterfaceController {

    @IBOutlet weak var group: WKInterfaceGroup!
    @IBOutlet weak var hexLabel: WKInterfaceLabel!
    var dataManager = DataManager()
    var hex: String = ""

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
//        if let contextHex = context as? String {
//            hex = contextHex
//            group.setBackgroundColor(UIColor(hexString: hex))
//            hexLabel.setText(hex.uppercased())
//        }
        setColor()
    }
    override func didAppear() {
        setColor()
    }
    
    @IBAction func saveButtonTapped() {
        dataManager.addData(hex: hex)
        self.popToRootController()
        self.dismiss()
    }
    
    override func willActivate() {
        super.willActivate()
        setColor()
    }

    override func didDeactivate() {
        super.didDeactivate()
        resetColor()
    }
    
    func setColor() {
        getHex()
        group.setBackgroundColor(UIColor(hexString: hex))
        hexLabel.setText(hex.uppercased())
    }
    
    func getHex() {
        if MyColorModel.hex.isEmpty {
            hex = UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1).toHexString()
        } else {
            hex = MyColorModel.hex
        }
    }
    
    func resetColor() {
        MyColorModel.hex = ""
    }

}
