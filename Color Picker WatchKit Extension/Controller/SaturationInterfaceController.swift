//
//  SaturationInterfaceController.swift
//  NanoChallenge WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit
import Foundation

class SaturationInterfaceController: WKInterfaceController {
    @IBOutlet weak var group: WKInterfaceGroup!
    var crownAccumulator = 0.0
    var satVal: CGFloat = 1.0 {
        didSet {
            if satVal < 0.0 || satVal > 1.0 {
                satVal = oldValue
            }
            group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: satVal, brightness: MyColorModel.brightness, alpha: 1))
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
    }
    
    override func willActivate() {
        super.willActivate()
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
        crownSequencer.focus()
    }
    
    override func didDeactivate() {
    }
    
    override func didAppear() {
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
    }
    
    override func willDisappear() {
        MyColorModel.saturation = satVal
        crownSequencer.resignFocus()
    }
    
}

extension SaturationInterfaceController: WKCrownDelegate {
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1 {
            satVal += 0.05
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            satVal -= 0.05
            crownAccumulator = 0.0
        }
    }
}

