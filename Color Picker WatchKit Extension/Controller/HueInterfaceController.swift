//
//  InterfaceController.swift
//  NanoChallenge WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit
import Foundation


class HueInterfaceController: WKInterfaceController {

    @IBOutlet weak var group: WKInterfaceGroup!
    var crownAccumulator = 0.0
    var hueVal: CGFloat = 0.0 {
        didSet {
            if hueVal < 0.0 || hueVal > 1.0 {
                hueVal = oldValue
            }
            group.setBackgroundColor(UIColor(hue: hueVal, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
    }
    
    override func willActivate() {
        super.willActivate()
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
        crownSequencer.focus()
    }
    
    override func didDeactivate() {
    }
    
    override func didAppear() {
        group.setBackgroundColor(UIColor(hue: MyColorModel.hue, saturation: MyColorModel.saturation, brightness: MyColorModel.brightness, alpha: 1))
    }
    
    override func willDisappear() {
        MyColorModel.hue = hueVal
        crownSequencer.resignFocus()
    }
}

extension HueInterfaceController: WKCrownDelegate {
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        crownAccumulator += rotationalDelta

        if crownAccumulator > 0.1 {
            hueVal += 0.01
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            hueVal -= 0.01
            crownAccumulator = 0.0
        }
    }
}
