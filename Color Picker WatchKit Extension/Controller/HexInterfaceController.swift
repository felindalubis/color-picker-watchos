//
//  HexInterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 23/08/21.
//

import WatchKit
import Foundation


class HexInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var textField: WKInterfaceTextField!
    var hex = "#000000"
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
    }
    @IBAction func textFieldAction(_ value: NSString?) {
        hex = "\("#\(value ?? "000000")".uppercased())"
        textField.setText(hex)
    }
    
    @IBAction func getColorButtonTapped() {
        MyColorModel.hex = hex
        self.pushController(withName: "saveView", context: nil)
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
