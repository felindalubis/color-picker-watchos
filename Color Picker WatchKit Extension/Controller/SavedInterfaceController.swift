//
//  SavedInterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit
import Foundation
import CoreData

class SavedInterfaceController: WKInterfaceController {
    @IBOutlet weak var colorTable: WKInterfaceTable!
    var dataManager = DataManager()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        setTable()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    override func didAppear() {
        dataManager.loadData()
        setTable()
    }
    
    func setTable() {
        colorTable.setNumberOfRows(dataManager.colorArr.count, withRowType: "Row")
        for (index, color) in dataManager.colorArr.enumerated() {
            guard let row = colorTable.rowController(at: index) as? ColorRow, let hex = color.hex else { continue }
            row.hexLabel.setText(hex.uppercased())
            row.group.setBackgroundColor(UIColor(hexString: hex))
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "detail", context: dataManager.colorArr[rowIndex])
    }

}
