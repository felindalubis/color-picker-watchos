//
//  ColorDetailInterfaceController.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 19/08/21.
//

import WatchKit
import Foundation
import CoreData

class ColorDetailInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var group: WKInterfaceGroup!
    @IBOutlet weak var hexLabel: WKInterfaceLabel!
    var dataManager = DataManager()
    var selectedColor: Color?
    
    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        if let context = context as? Color {
            selectedColor = context
            group.setBackgroundColor(UIColor(hexString: selectedColor?.hex ?? "#000000"))
            hexLabel.setText(context.hex)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }

    @IBAction func deleteButtonPressed() {
        let delete = WKAlertAction(title: "Delete", style: .destructive) {
            //delete core data
            self.dataManager.deleteData(color: self.selectedColor!)
            self.pop()
        }
        let cancel = WKAlertAction(title: "Cancel", style: .cancel) {
            self.dismiss()
        }
        presentAlert(withTitle: nil, message: "This color will be removed from your saved list", preferredStyle: .alert, actions: [delete, cancel])
    }
}
