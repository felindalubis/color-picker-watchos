//
//  MyColorModel.swift
//  NanoChallenge WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import UIKit

struct MyColorModel {
    static var hue: CGFloat = 1.0
    static var saturation: CGFloat = 1.0
    static var brightness: CGFloat = 1.0
    
    static var hex: String = ""
}
