//
//  DataManager.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 19/08/21.
//

import Foundation
import WatchKit
import CoreData

class DataManager {
    var context = (WKExtension.shared().delegate as! ExtensionDelegate).persistentContainer.viewContext
    var colorArr = [Color]()
    
    init() {
        loadData()
    }

    func loadData() {
        let request: NSFetchRequest<Color> = Color.fetchRequest()
        do {
            colorArr = try context.fetch(request)
        } catch {
            print("Error fetching data: \(error.localizedDescription)")
        }
    }
    
    func addData(hex: String) {
        let newItem = Color(context: context)
        newItem.hex = hex
        save()
    }
    
    func deleteData(color: Color) {
        context.delete(color)
        save()
    }
    
    
    func save() {
        do {
            try context.save()
            print("Successfully save")
        } catch {
            print("Can't save color, \(error.localizedDescription)")
        }
    }
    
}
