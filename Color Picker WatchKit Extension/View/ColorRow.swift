//
//  ColorRow.swift
//  Color Picker WatchKit Extension
//
//  Created by Felinda Gracia Lubis on 18/08/21.
//

import WatchKit

class ColorRow: NSObject {
    @IBOutlet weak var hexLabel: WKInterfaceLabel!
    @IBOutlet weak var group: WKInterfaceGroup!
}
